﻿using System;

namespace Quicksort
{
    class Program
    {
        private static readonly QuicksortService QuicksortService = new QuicksortService();

        static void Main(string[] args)
        {
            // Create an unsorted array of string elements
            IComparable[] unsorted = { "z", "e", "x", "c", "m", "q", "a" };

            // Print the unsorted array
            foreach (var t in unsorted)
            {
                Console.Write(t + " ");
            }

            Console.WriteLine();

            // Sort the array
            QuicksortService.Quicksort(unsorted, 0, unsorted.Length - 1);

            // Print the sorted array
            foreach (var t in unsorted)
            {
                Console.Write(t + " ");
            }

            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
