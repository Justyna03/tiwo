﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Quicksort.Test
{
    [TestClass]
    public class QuickSortTests
    {
        [TestMethod]
        public void Quicksort()
        {
            IComparable[] entry = { -1000, -450, -100, -9, 0, 1, 3, 100, 500, 750 };

            var unsorted = entry.OrderBy(x => new Guid()).ToArray();

            var quicksortService = new QuicksortService();

            // Sort the array
            quicksortService.Quicksort(unsorted, 0, unsorted.Length - 1);

            unsorted.ShouldBeEquivalentTo(entry, config => config.WithStrictOrdering());
        }
    }
}
